import Head from "next/head";
import Heading from "../components/Heading";

const Error = () => (
  <>
    <Head>
      <title>Error</title>
    </Head>
    <Heading text="404" />
    <Heading tag="h2" text="Something is going wrong..." />
  </>
);

export default Error;
