import Head from "next/head";
import Heading from "../components/Heading";
import Socials from "../components/Socials";

const getData = async () => {
  const response = await fetch(`${process.env.API_HOST}/socials`);
  return await response.json();
};

const Home = async () => {
  const data = await getData();
  return (
    <>
      <Head>
        <title>Home</title>
      </Head>
      <Heading text="Home" />
      <Socials data={data} />
    </>
  );
};

export default Home;
