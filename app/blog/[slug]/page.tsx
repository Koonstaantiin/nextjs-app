import Link from "next/link";

export const dynamicParams = true;

export async function generateStaticParams() {
  return [1];
}

export default function Page({ params }: any) {
  const { slug } = params;
  return (
    <>
      <p>
        Blog post: {slug || "index"},{" "}
        <Link href="/blog" as="/blog">
          Go to blog index
        </Link>
      </p>
      <p>
        <Link href="/blog/test" as="/blog/test">
          Go to blog test
        </Link>
      </p>
    </>
  );
}
