import Heading from "@/components/Heading";
import Head from "next/head";

const Company = () => (
  <>
    <Head>
      <title>Posts</title>
    </Head>
    <Heading text="Company" />
    <p>
      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse
      ultrices malesuada turpis. Phasellus tincidunt orci tortor, ut faucibus
      felis consectetur at. Proin aliquam finibus nunc ac fermentum. Nam
      convallis condimentum tempus. Maecenas auctor posuere urna, ut aliquam
      dolor. Vivamus quis odio mollis, semper dolor sit amet, malesuada turpis.
      Aliquam erat volutpat. Etiam erat odio, pharetra at tortor vitae, pharetra
      pharetra nunc. Duis pretium tristique diam, et malesuada lacus facilisis
      id. Praesent placerat rhoncus cursus. Ut molestie sollicitudin augue
      posuere posuere. Nulla mattis, elit at feugiat fermentum, eros augue
      ullamcorper sem, sed malesuada magna odio ut massa. Praesent non ligula
      luctus leo euismod finibus. In id convallis purus. Pellentesque venenatis
      condimentum semper.
    </p>

    <p>
      Nullam id ornare mauris, eu lobortis purus. Duis a malesuada lorem. Nunc
      sed dignissim ipsum. Sed nisl ex, ornare et velit et, sodales congue
      tortor. Suspendisse eget nisl a massa ultrices malesuada nec eget elit. Ut
      vestibulum cursus lorem in viverra. Ut rhoncus viverra erat, at porta arcu
      cursus aliquam. Nulla rhoncus eros ac lectus venenatis finibus. Aliquam
      eget massa pellentesque, hendrerit erat ac, malesuada lorem. Curabitur
      massa tortor, fermentum sit amet pellentesque at, imperdiet nec felis. In
      pellentesque pulvinar venenatis. Maecenas rhoncus ultricies ligula, a
      vestibulum ipsum porttitor non. Integer blandit mollis nisi eu porta.
    </p>
  </>
);

export default Company;

// Learn next:
// https://nextjs.org/learn/basics/dynamic-routes
