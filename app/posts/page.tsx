import Head from "next/head";
import Heading from "@/components/Heading";
import PostsOutput from "@/components/Posts";
import { Posts } from "@/components/Posts/interfaces";
import { getPosts } from "@/pages/api/posts";

const Posts = async () => {
  const data: Posts = await getPosts();
  return (
    <>
      <Head>
        <title>Posts</title>
      </Head>
      <Heading text="Posts" />
      <PostsOutput data={data} />
    </>
  );
};

export default Posts;
