import Link from "next/link";

export const dynamicParams = true;

export async function generateStaticParams() {
  return [1];
}

export default function Page({ params }: any) {
  const { id } = params;
  return (
    <>
      <p>
        Post id: {id},{" "}
        <Link href="/posts" as="/posts">
          Go to posts index
        </Link>
      </p>
      <p>
        <Link href="/posts/2" as="/posts/2">
          Go to post #2
        </Link>
      </p>
    </>
  );
}
