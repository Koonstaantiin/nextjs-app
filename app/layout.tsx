"use client";

import Footer from "@/components/Footer";
import Header from "@/components/Header";
import { ReactNode } from "react";
import styles from "@/styles/layout.module.scss";

type AppPropsWithLayout = {
  children: ReactNode;
};

export default function App({ children }: AppPropsWithLayout) {
  return (
    <html lang="en">
      <body>
        <main className={styles.wrapper}>
          <Header />
          {children}
          <Footer />
        </main>
      </body>
    </html>
  );
}
