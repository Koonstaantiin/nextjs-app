"use client";

import { useState } from "react";
import Link from "next/link";

const Header = ({ title }: { title: string }) => {
  return <h1>{title ? title : "Default title"}</h1>;
};

const HomePage = () => {
  const names = ["Ada Lovelace", "Grace Hopper", "Margaret Hamilton"];

  const [likes, setLikes] = useState(0);

  function handleClick() {
    setLikes((value) => value + 1);
  }

  return (
    <div>
      <Header title="Develop. Preview. Ship. 🚀" />
      <ul>
        {names.map((name) => (
          <li key={name}>{name}</li>
        ))}
      </ul>

      <button onClick={handleClick}>Like ({likes})</button>
      <div>
        <Link href="/blog/test" as={"/blog/test"} replace={false}>
          Test blog link
        </Link>
      </div>
    </div>
  );
};

export default HomePage;
