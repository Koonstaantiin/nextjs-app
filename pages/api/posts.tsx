import { NextApiRequest, NextApiResponse } from "next";

export async function getPosts() {
  const response = await fetch("https://jsonplaceholder.typicode.com/posts", {
    cache: "no-store",
  });
  if (response.status !== 200) {
    throw new Error(`Status ${response.status}`);
  }
  return response.json();
}

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const response = await getPosts();
  return res.status(200).json(response);
}
