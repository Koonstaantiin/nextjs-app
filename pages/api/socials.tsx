import { NextApiRequest, NextApiResponse } from "next";

const SOCIALS_MOCK = [
  {
    id: 1,
    title: "Youtube",
    path: "https://youtube.com/",
  },
  {
    id: 2,
    title: "Instagram",
    path: "https://instagram.com/",
  },
  {
    id: 3,
    title: "LinkedIn",
    path: "https://linkedin.com/",
  },
  {
    id: 4,
    title: "Facebook",
    path: "https://facebook.com/",
  },
  {
    id: 5,
    title: "Twitter",
    path: "https://twitter.com/",
  },
];

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  if (req.method === "GET") {
    res.status(200).json(SOCIALS_MOCK);
  }
}
