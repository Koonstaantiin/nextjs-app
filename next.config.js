const path = require("path");

module.exports = {
  experimental: {
    forceSwcTransforms: true,
    appDir: true,
  },
  sassOptions: {
    includePaths: [path.join(__dirname)],
  },
};
