import Image from "next/image";
import { usePathname } from "next/navigation";
import Link from "next/link";
import styles from "./styles.module.scss";
import cs from "classnames";

const navigation = [
  { id: 1, title: "Home", path: "/" },
  { id: 2, title: "About", path: "/about" },
  { id: 3, title: "Company", path: "/company" },
  { id: 4, title: "Blog", path: "/blog" },
  { id: 5, title: "Posts", path: "/posts" },
];

const View = () => {
  const pathname = usePathname();

  return (
    <nav className={styles.nav}>
      <div>
        <Image src="/logo.png" width={60} height={60} alt="Test project" />
      </div>
      <div className={styles.links}>
        {navigation.map(({ id, title, path }) => (
          <Link key={id} href={path} legacyBehavior>
            <a
              className={cs(styles.link, {
                [styles.link_active]: pathname === path,
              })}
            >
              {title}
            </a>
          </Link>
        ))}
      </div>
    </nav>
  );
};

export default View;
