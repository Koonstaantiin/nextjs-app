import Heading from "./Heading";

const Footer = () => (
  <footer>
    <Heading tag="h3" text="Created by Kostiantyn" />
  </footer>
);

export default Footer;
