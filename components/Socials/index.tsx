import styles from "./styles.module.scss";
import type { ViewProps } from "./interfaces";

const View = ({ data }: ViewProps) => {
  if (!data) {
    return null;
  }
  return (
    <>
      <h3>Socials list from fetch</h3>
      <ul className={styles["socials-list"]}>
        {data?.map(({ id, title, path }) => (
          <li key={id}>
            <a href={path} target="_blank" rel="noopener noreferrer">
              {title}
            </a>
          </li>
        ))}
      </ul>
    </>
  );
};

export default View;
