export interface Social {
  id: number;
  title: string;
  path: string;
}

export type Socials = Array<Social>;

export type ViewProps = {
  data: Socials;
};
