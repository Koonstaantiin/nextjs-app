import Link from "next/link";
import { ViewProps } from "./interfaces";

const View = async ({ data }: ViewProps) => {
  if (!data) {
    return null;
  }

  return (
    <table>
      <thead>
        <tr>
          <td>ID</td>
          <td>UserID</td>
          <td>Title</td>
          <td>Body</td>
        </tr>
      </thead>
      <tbody>
        {Array.isArray(data) &&
          data.map(({ id, userId, title, body }) => (
            <tr key={id}>
              <td>
                <Link href={`/posts/${id}`} as={`/posts/${id}`}>
                  {id}
                </Link>
              </td>
              <td>{userId}</td>
              <td>{title}</td>
              <td>{body}</td>
            </tr>
          ))}
      </tbody>
    </table>
  );
};

export default View;
